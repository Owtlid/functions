// 1. Check Winner Team

// ორი გუნდი ეჯიბრება ერთმანეთს კერლინგში, გუნდი PowerRangers და გუნდი FairyTails. თითოეული გუნდი ეჯიბრება ერთმანეთს 3-3 ჯერ, შემდეგ კი ითვლება ამ ქულების საშუალო (თითოეულ გუნდს თავისი საშუალო აქვს), გუნდი იგებს მხოლოდ მაშინ, თუ კი მისი საშუალო ორჯერ მაინც იქნება მეტი მეორე გუნდის საშუალოზე, წინააღმდეგ შემთხვევაში, გამარჯვებული არავინ არ არის. თქვენი დავალებაა:
// 1.1. შექმენათ ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
// 1.2. გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
// 1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner),  მიიღებს ორ პარამეტრს გუნდების საშუალოს სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით (მაგ. პირველმა გუნდმა გაიმარჯვა 31 vs 15 ქულა).
// 1.4 გამოყენეთ ეს ფუნქცია რომ გამოავლინოთ გამარჯვებული გუნდი 1. PowerRangers (44, 23, 71) , FairyTails (65, 54, 49) 2. PowerRangers (85, 54, 41) , FairyTails (23, 34, 47)
// 1.5 optional : ფუნქციაში შეგიძლიათ გაითვალისწინოთ ყაიმის შემთხვევაც.
const Teams = [];
let PowerRangers = {
    name: "PowerRangers",
    roundOneScores: [44, 23, 71],
    roundTwoScores: [85, 54, 41],
};
let FairyTails = {
    name: "FairyTails",
    roundOneScores: [65, 54, 49],
    roundTwoScores: [23, 34, 47],
};

function calcRoundOneAverage (obj) {
    let sum = 0;
    for (let i = 0; i < obj.roundOneScores.length; i++){
        sum +=  obj.roundOneScores[i];
    }
    return sum/obj.roundOneScores.length;
};

function calcRoundTwoAverage (obj) {
    let sum = 0;
    for (let i = 0; i < obj.roundTwoScores.length; i++){
        sum +=  obj.roundTwoScores[i];
    }
    return sum/obj.roundTwoScores.length;
};

PowerRangers.roundOneAvg = calcRoundOneAverage(PowerRangers);
PowerRangers.roundTwoAvg = calcRoundTwoAverage(PowerRangers);

FairyTails.roundOneAvg = calcRoundOneAverage(FairyTails);
FairyTails.roundTwoAvg = calcRoundTwoAverage(FairyTails);


PowerRangers.unitedAvg =( PowerRangers.roundOneAvg + PowerRangers.roundTwoAvg ) / 2;
FairyTails.unitedAvg =( FairyTails.roundOneAvg + FairyTails.roundTwoAvg ) / 2;

console.log(PowerRangers, FairyTails);

function winner(teamOne, teamTwo) {
    if(teamOne.unitedAvg > (teamTwo.unitedAvg * 2)){
        console.log(`Team ${teamOne.name} won the Tournament with ${teamOne.unitedAvg} points`);
    }else if(teamTwo.unitedAvg > (teamOne.unitedAvg * 2)){
        console.log(`Team ${teamTwo.name} won the Tournament with ${teamTwo.unitedAvg} points`);
    }else if(teamOne.unitedAvg === teamTwo.unitedAvg){
        console.log(`Its Tie! Team ${teamOne.name} have ${teamOne.unitedAvg} points and Team ${teamTwo.name} have ${teamTwo.unitedAvg} points`);
    }else{
        console.log("X_X");
    }
};
winner(PowerRangers, FairyTails);

Teams[0] = PowerRangers;
Teams[1] = FairyTails;

// 2. Tip Calculator

// ლუდოვიკო ხშირად დადის რესტორნებში და უნდა რომ მოიფიქროს კალკულატორი იმისთვის, რომ დაითვალოს თუ რამდენი უნდა დატოვოს ჩაის ფული. მას აქვს ასეთი წესი: თიფი 15% - ია იმ შემთხვევაში თუ ანგარიში 50 დან 300 მერყეობს, ხოლო თუ სხვა რეინჯშია, მაშინ თიფი 20% - ია. თქვენი დავალებაა:

// 2.1. შექმენათ ფუქნცია (calcTip) რომელიც მიიღებს ანგარიშის მნიშნელობას პარამეტრად და დააბრუნებს თიფს ზემოთხსენებული წესების მიხედვით
// 2.2. შექმეათ მასივი სადაც შეინახავთ ანგარიშებს : 22, 295, 176, 440, 37, 105, 10, 1100, 96, 52 (აქ ლუფით შენახვა არაა აუცილებელი, ეს მოცემული დატაა და პირდაპირ შეგიძლიათ ჩაწეროთ ერეიში)
// 2.3. შექმენათ თიფების მასივი და რომელშიც შეინახავთ ფუნქციის მეშვეობით დათვლის თიფებს.
// 2.4. შექმენათ მასივი სადაც შეინახავთ ჯამურ თანხას თითოეული ანგარიში + ანგარიშის შესაბამისი თიფი.
// 2.5. გამოიყენოთ ლუფი კალკულაციების ჩასატარებლად.
// 2.6. შექმენათ ფუნქცია რომელიც მიიღებს მასივს პარამეტრად და დააბრუნებს საშუალოს, ამ ფუნქციით უნდა დავითვალოთ საშუალოდ რამდენ ჩაის ტოვებს ჩვენი ლუდოვიკო (2.3. პუნტში მიღებული მასივი) და ასევე უნდა დავითვალოთ საშუალოდ რა თანხა დახარჯა მანდ ჭამა-სმაში (2.4. პუნქტის მასივის საშუალო არითმეტიკული)

// note: არ არის აუცილებელი ეს სამი მასივი იყოს ცალ-ცალკე, მოიფიქრეთ ოპტიმალური დატის სტრუქტურა, თუ სად და როგორ შეგიძლიათ შეინახოთ ყველა ეს მნიშვნელობა.
// note2: მასივებში ინფოს დასამატებლად შეგიძლიათ გამოიყენოთ push მეთოდი.

let Ludovico = {
    name: "Ludovico",
    bill: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
    tips: [],
    total: [],
};

function calcTip(obj) {
    for(let i = 0; i < obj.bill.length; i++){
        if(obj.bill[i] >= 50 && obj.bill[i] <= 300){
            obj.tips[i] = obj.bill[i] * 15/100;
        }else{
            obj.tips[i] = obj.bill[i] * 20/100;
        }
    }
};

function calcTotal(obj) {
    for(let i = 0; i < obj.bill.length; i++){
        obj.total[i] = obj.tips[i] + obj.bill[i];
    }
}

function calcAvg(arr) {
    let sum = 0;
    for(let i = 0; i < arr.length; i++){
        sum += arr[i];
    }
    return sum/arr.length;
}

calcTip(Ludovico);
calcTotal(Ludovico);
Ludovico.averageTips = calcAvg(Ludovico.tips);
Ludovico.averageTotal = calcAvg(Ludovico.total);

console.log(Ludovico);